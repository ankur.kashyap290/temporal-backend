const express = require('express');
const router = express.Router();
const { Op } = require('sequelize');
const models = require('../models');
const { resultOk, resultError } = require('../misc/common');
// const auth = require('../auth/auth');
const Sequelize = require('Sequelize');

router.post('/create-record', async function(req, res, next) {
	const { databaseName, tableName, ...rest } = req.body;
	try {
		const db = new Sequelize(databaseName, 'root', null, {
			host: 'localhost',
			dialect: 'mysql',
		});
		let keys = Object.keys(rest).toString();
		const values = Object.values(rest).map((item) => `'${item}'`);
		const inserted = await db.query(
			`INSERT INTO ${tableName}(${keys}) VALUES (${values.toString()});`
		);
		const columns = await db.query(
			`select * FROM ${databaseName}.${tableName};`
		);
		if (inserted === null) {
			return res.json(resultError(`Table not found with "${tableName}" name`));
		} else {
			const exist = await models.sequelize.query(
				`select * FROM information_schema.COLUMNS where TABLE_SCHEMA = '${databaseName}' AND TABLE_NAME = '${tableName}_hist';`
			);
			let history;

			if (exist[0].length === 0) {
				history = await db.query(
					`CREATE TABLE IF NOT EXISTS ${tableName}_hist AS
				SELECT * FROM ${tableName} WHERE 1=2;`
					// `CREATE TABLE IF NOT EXISTS ${tableName}_hist AS
					// SELECT ${keys.concat(',id')} FROM ${tableName} WHERE 1=2;`
				);
				if (history) {
					const history1 = await db.query(
						`ALTER TABLE ${tableName}_hist ADD COLUMN hist_id int AUTO_INCREMENT PRIMARY KEY, ADD COLUMN start_date DATETIME NOT NULL DEFAULT NOW(), ADD COLUMN end_date DATETIME NULL;`
					);
				}
			}
			await db.query(
				`INSERT INTO ${tableName}_hist(${keys.concat(
					',id'
				)}) VALUES (${values
					.toString()
					.concat(`,'${columns[0][columns[0].length - 1].id}'`)});`
			);

			return res.json(resultOk(columns, 'List of data'));
		}
	} catch (error) {
		next(error);
	}
});

router.post('/update-record', async function(req, res, next) {
	const { databaseName, tableName, id, ...rest } = req.body;
	try {
		const db = new Sequelize(databaseName, 'root', null, {
			host: 'localhost',
			dialect: 'mysql',
		});
		const exist = await models.sequelize.query(
			`select * FROM information_schema.COLUMNS where TABLE_SCHEMA = '${databaseName}' AND TABLE_NAME = '${tableName}_hist';`
		);
		let history;
		if (exist[0].length === 0) {
			history = await db.query(
				`CREATE TABLE IF NOT EXISTS ${tableName}_hist AS
				SELECT * FROM ${tableName} WHERE 1=2;`
			);
			if (history) {
				const history1 = await db.query(
					`ALTER TABLE ${tableName}_hist ADD COLUMN hist_id int AUTO_INCREMENT PRIMARY KEY, ADD COLUMN start_date DATETIME NOT NULL DEFAULT NOW(), ADD COLUMN end_date DATETIME NULL;`
				);
			}
		}
		await db.query(
			`INSERT INTO ${tableName}_hist SELECT *, '', NOW(), NULL FROM ${tableName} WHERE id=${id};`
			// `INSERT INTO ${tableName}_hist SELECT *, NULL AS end_date, NOW() AS start_date, 6 as hist_id FROM ${tableName} WHERE id=${id};`
		);
		let inserted;

		for (const [key, value] of Object.entries(rest)) {
			console.log(`${key}= ${value}`);
			inserted = await db.query(
				`UPDATE ${tableName} SET ${key}='${value}' WHERE id=${id}`
			);
		}
		const columns = await db.query(
			`select * FROM ${databaseName}.${tableName};`
		);
		if (inserted === null) {
			return res.json(resultError(`Table not found with "${tableName}" name`));
		} else {
			return res.json(resultOk(columns, 'List of data'));
		}
	} catch (error) {
		next(error);
	}
});

router.post('/delete-record', async function(req, res, next) {
	const { databaseName, tableName, id } = req.body;
	try {
		const db = new Sequelize(databaseName, 'root', null, {
			host: 'localhost',
			dialect: 'mysql',
		});
		const exist = await models.sequelize.query(
			`select * FROM information_schema.COLUMNS where TABLE_SCHEMA = '${databaseName}' AND TABLE_NAME = '${tableName}_hist';`
		);
		let history;
		if (exist[0].length === 0) {
			history = await db.query(
				`CREATE TABLE IF NOT EXISTS ${tableName}_hist AS
				SELECT * FROM ${tableName} WHERE 1=2;`
			);
			if (history) {
				const history1 = await db.query(
					`ALTER TABLE ${tableName}_hist ADD COLUMN hist_id int AUTO_INCREMENT PRIMARY KEY, ADD COLUMN start_date DATETIME NOT NULL DEFAULT NOW(), ADD COLUMN end_date DATETIME NULL;`
				);
			}
		}
		await db.query(
			`INSERT INTO ${tableName}_hist SELECT *, '', NOW(), NOW() FROM ${tableName} WHERE id=${id};`
		);
		let deleted = await db.query(`DELETE FROM ${tableName} WHERE id=${id};`);

		const columns = await db.query(
			`select * FROM ${databaseName}.${tableName};`
		);
		if (deleted === null) {
			return res.json(resultError(`Table not found with "${tableName}" name`));
		} else {
			return res.json(resultOk(columns, 'List of data'));
		}
	} catch (error) {
		next(error);
	}
});

router.get('/', async function(req, res, next) {
	let dbs = null;
	try {
		dbs = await models.sequelize.query('show databases');
		return res.json(resultOk(dbs, 'List of databases'));
	} catch (error) {
		next(error);
	}
});

router.get('/database/:dbname', async function(req, res, next) {
	let tables = null;
	const name = req.params.dbname;
	try {
		tables = await models.sequelize.query(
			`select * FROM information_schema.TABLES where TABLE_TYPE = 'BASE TABLE' and TABLE_SCHEMA = '${name}';`
		);
		if (tables === null) {
			return res.json(resultError(`Database not found with "${name}" name`));
		} else {
			return res.json(resultOk(tables, 'List of tables'));
		}
	} catch (error) {
		next(error);
	}
});

router.get('/table/:dbname/:tablename', async function(req, res, next) {
	let columns = null;
	const dbname = req.params.dbname;
	const name = req.params.tablename;
	try {
		columns = await models.sequelize.query(
			`select * FROM information_schema.COLUMNS where TABLE_SCHEMA = '${dbname}' AND TABLE_NAME = '${name}';`
		);
		if (columns === null) {
			return res.json(resultError(`Table not found with "${name}" name`));
		} else {
			return res.json(resultOk(columns, 'List of columns'));
		}
	} catch (error) {
		next(error);
	}
});

router.get('/tabledata/:databasename/:tablename', async function(
	req,
	res,
	next
) {
	try {
		let columns = null;
		const databasename = req.params.databasename;
		const tablename = req.params.tablename;
		const db = new Sequelize(databasename, 'root', null, {
			host: 'localhost',
			dialect: 'mysql',
		});
		columns = await db.query(`select * FROM ${databasename}.${tablename};`);
		if (columns === null) {
			return res.json(resultError(`Table not found with "${tablename}" name`));
		} else {
			return res.json(resultOk(columns, 'List of data'));
		}
	} catch (error) {
		next(error);
	}
});

router.delete('/:id', async function(req, res) {
	const id = req.params.id;
	const deleted = await models.Category.destroy({
		where: { id: id },
	});
	if (deleted) {
		res.json(resultOk(deleted, 'Category is deleted successfully'));
	} else {
		res.json(resultError(`Category not found with "${id}" id`));
	}
});
module.exports = router;
