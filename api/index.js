const databases = require('./databases');
const user = require('./user');
const subcategory = require('./subcategory');

const handleError = function(err, req, res) {
	if (err) {
		res.json({ status: 'error', data: null, error: err.message });
	}
};

const api = function(server) {
	server.use('/api/databases', databases, handleError);
	server.use('/api/subcategories', subcategory, handleError);
	server.use('/api/users', user, handleError);
};

module.exports = api;
