const express = require('express');
const router = express.Router();
const { Op } = require('sequelize');
const models = require('../models');
const { resultOk, resultError } = require('../misc/common');
// const auth = require('../auth/auth');

router.post('/', async function(req, res) {
  const data = req.body;
  const [subcategory, created] = await models.SubCategory.findOrCreate({
    where: { name: data.name },
  });
  if (created) {
    res.json(resultOk(subcategory.toJSON(), 'SubCategory is added successfully'));
  } else {
    res.json(resultError(null, `SubCategory already exists with name "${data.name}"`));
  }
});

router.post('/:id', async function(req, res) {
  const data = req.body;
  const id = req.params.id;
  let subcategory = await models.SubCategory.findOne({ where: { id: id } });
  if (subcategory !== null) {
    const alreadyExistCategory = await models.SubCategory.findOne({
      where: { [Op.and]: [{ name: data.name }, { id: { [Op.ne]: id } }] },
    });
    if (alreadyExistCategory === null) {
      const [updated] = await models.SubCategory.update(data, {
        where: { id: id },
      });
      if (updated) {
        const updatedCategory = await models.SubCategory.findOne({ where: { id: id } });
        res.json(resultOk(updatedCategory.toJSON(), 'SubCategory is updated successfully'));
      }
    } else {
      res.json(resultError(null, `SubCategory already exists with name "${data.name}"`));
    }
  } else {
    res.json(resultError(null, `SubCategory does not exists with id "${id}"`));
  }
});

router.delete('/:id', async function(req, res) {
  const id = req.params.id;
  const deleted = await models.SubCategory.destroy({
    where: { id: id },
  });
  if (deleted) {
    res.json(resultOk(deleted, 'SubCategory is deleted successfully'));
  } else {
    res.json(resultError(`SubCategory not found with "${id}" id`));
  }
});

router.get('/', async function(req, res, next) {
  let subcategories = null;
  try {
    subcategories = await models.SubCategory.findAll({});
    return res.json(resultOk(subcategories, 'List of subcategories'));
  } catch (error) {
    next(error);
  }
});

router.get('/:id', async function(req, res, next) {
  let subcategory = null;
  const id = req.params.id;
  try {
    subcategory = await models.SubCategory.findOne({ where: { id: id } });
    if (subcategory === null) {
      return res.json(resultError(`SubCategory not found with "${id}" id`));
    } else {
      return res.json(resultOk(subcategory, 'Founded subcategory'));
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
