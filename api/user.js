const express = require('express');
const router = express.Router();
const moment = require('moment');
const md5 = require('md5');
const { Op } = require('sequelize');
const models = require('../models');
const jwt = require('../misc/jwt_token');
const { resultError, resultOk } = require('../misc/common');
const auth = require('../misc/auth');

router.post('/', async function(req, res, next) {
	const userData = req.body;
	console.log(userData, req);
	let data = {
		fullName: userData.fullName,
		email: userData.email,
		password: md5(userData.password),
	};
	try {
		let user = await models.User.findOne({
			where: { email: data.email },
		});
		if (user === null) {
			user = await models.User.create(data);
			res.json(resultOk(user, 'User Created Successfully'));
		} else {
			res.json(resultError(null, 'User Already Exist'));
		}
	} catch (error) {
		next(error);
	}
});

router.post('/sign-in', async function(req, res, next) {
	const userData = req.body;
	try {
		const email = userData.email;
		const password = md5(userData.password);
		const validUser = await models.User.findOne({
			where: { email: email, password: password },
		});
		if (validUser !== null) {
			let token = jwt.genJWTToken(validUser);
			validUser.lastLoggedIn = moment()
				.utc()
				.unix();
			validUser.save();
			return res.json(resultOk({ validUser, token }, 'Login Successfully'));
		} else {
			return res.json(resultError(null, 'Invalid Email or Password'));
		}
	} catch (error) {
		next(error);
	}
});

module.exports = router;
