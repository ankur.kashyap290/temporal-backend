const resultOk = function(data, msg) {
	return { status: 'ok', data, msg };
};

const resultError = function(data, msg) {
	return { status: 'error', data, msg };
};

module.exports = { resultError, resultOk };
