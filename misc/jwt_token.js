const jwt = require('jsonwebtoken');

class JwtToken {
	static genJWTToken(user) {
		let payload = {
			iss: 'Temporal',
			sub: user.id,
			user_id: user.id,
			email: user.email,
			fullName: user.fullName,
		};
		let token = jwt.sign(payload, '--Temporal--', { expiresIn: '60 days' });
		return token;
	}

	static verifyJWTToken(token) {
		return new Promise((resolve, reject) => {
			jwt.verify(token, '--Temporal--', (err, decodedToken) => {
				if (err || !decodedToken) {
					return reject(err);
				}
				resolve(decodedToken);
			});
		});
	}
	static genJWTgenSubscriberJWTTokenToken(user) {
		let payload = {
			iss: 'Temporal',
			sub: user.id,
			user_id: user.id,
			email: user.email,
			fullName: user.fullName,
		};
		let token = jwt.sign(payload, '--Temporal--', { expiresIn: '60 days' });
		return token;
	}
}

module.exports = JwtToken;
